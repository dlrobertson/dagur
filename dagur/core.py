# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import queue
import urllib.request
import urllib.error
import time
import itertools
import datetime

from stem import Signal
import stem.connection

import pymongo.errors
from pymongo import MongoClient

import getpass

__all__ = [ "Website", "IncompleteWebsite", "CheckDuplicate", "TorEngine",
        "TimedWait", "MongoDBDump", "OnionGen" ]

class Website(object):
    def __init__(self, url, source):
        self.url = url
        self.source = source

class IncompleteWebsite(Website):
    def __init__(self, url):
        super(IncompleteWebsite, self).__init__(url, None)

class CheckDuplicate(Website):
    def __init__(self, url):
        super(CheckDuplicate, self).__init__(url, None)

DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7"

class TorEngine(object):
    def __init__(self, pw = None, control = ("127.0.0.1", 9051), signal = Signal.NEWNYM,
            proxy_handler = urllib.request.ProxyHandler({"http": "127.0.0.1:8118"}),
            data = None, headers = { "User-Agent": DEFAULT_USER_AGENT }):
        if pw:
            self.pw = pw
        else:
            self.pw = getpass.getpass("Tor password: ")
        self.data = data
        self.headers = headers
        self.control = control
        self.signal = signal
        self.proxy_handler = proxy_handler
        proxy_opener = urllib.request.build_opener(self.proxy_handler)
        urllib.request.install_opener(proxy_opener)

    def send_signal(self):
        conn = stem.connection.connect(
                control_port = self.control,
                password = self.pw
                )
        conn.signal(self.signal)
        conn.close()

    def get_page_source(self, url):
        try:
            if url:
                self.send_signal()
                req = urllib.request.Request(url, self.data, self.headers)
                res = urllib.request.urlopen(req)
                if res:
                    return common.Website(url, str(res.read()))
                else:
                    return None
            else:
                return None
        except urllib.error.HTTPError as e:
            time.sleep(2)
            return None

    def clone(self):
        return TorEngine(self.pw, self.control, self.signal, self.proxy_handler,
                self.data, self.headers)

class MongoDBDump(object):
    def __init__(self, host = "127.0.0.1", port = "27017", dbname = "crawler",
            colname = "spider"):
        url = "".join(["mongodb://", host, ":", port, "/"])
        self.conn = MongoClient(url)
        self.col = self.conn[dbname][colname]

    def dump(self, message):
        try:
            curr = self.col.find({"_id": {"$eq": message.url}}).limit(1)

            if curr.count() > 0:
                self.col.update_one(
                        { "_id": message.url },
                        { "$set": {
                            "dateRange.last": datetime.datetime.now(),
                            "source": message.source
                            } }
                        )
            else:
                self.col.insert_one({ "_id": message.url, "source": message.source })

        except pymongo.errors.DuplicateKeyError:
            pass

    def find_by_id(self, _id):
        return self.col.find({"_id": {"$eq": _id}})

class OnionGen(object):
    def __init__(self, shared_queue, priority = 3, length = 16):
        self.arr = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7' ]
        self.length = length
        self.priority = priority

        if not isinstance(shared_queue, queue.PriorityQueue):
            raise errors.OnionGenError("must use PriorityQueue for argument 2")

        # PriorityQueues are thread safe
        self.queue_ref = shared_queue

    def produce(self):
        for j in itertools.product(*itertools.repeat(self.arr, self.length)):
            onion = "".join(j)
            self.queue_ref.put(
                    (self.priority, "".join(["http://", onion, ".onion"]))
                    )
