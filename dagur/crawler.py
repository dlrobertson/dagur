# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from bs4 import BeautifulSoup
import threading
import queue
import re
import time

from . import containers
from . import errors
from . import core

def process_queue_item(item):
    if item:
        if len(item) == 2:
            site = core.IncompleteWebsite(item[1])
            return site
        else:
            raise errors.SearchWorkerError("incorrectly formatted item")
    return None

class SearchWorker(threading.Thread):
    def __init__(self, parent, group = None, name = None,
            args = (), kwargs = None):
        super(SearchWorker, self).__init__(group = group, name = name,
                args = args, kwargs = kwargs)
        self.parent = parent
        self.eng = parent.eng.clone()
        self.queue_ref = parent.queue_ref
        self.signal_stop = parent.signal_stop
        return

    def _process_queue_item(self, item):
        site = process_queue_item(item)
        if site and self.parent.notify(core.CheckDuplicate(site.url)):
            return site
        else:
            return None

    def run(self):
        while not self.signal_stop.is_set():
            while not self.queue_ref.empty():
                incmplt_site = self._process_queue_item(self.queue_ref.get())
                if incmplt_site:
                    site = self.eng.get_page_source(incmplt_site.url)
                    self.parent.notify(site)
                else:
                    self.parent.notify(incmplt_site)
        return

class OnionGenWrapper(threading.Thread):
    def __init__(self, queue_ref, signal_stop, prio = 3, length = 16,
            group = None, name = None, args = (), kwargs = None):
        super(OnionGenWrapper, self).__init__(group = group, name = name,
                args = args, kwargs = kwargs)
        self.onion_gen = core.OnionGen(queue_ref, prio, length)
        self.signal_stop = signal_stop

    def run(self):
        self.onion_gen.produce()
        self.signal_stop.set()

class Prioritizer(object):
    def __init__(self, re_dict):
        self.re_dict = re_dict
        self.worst = max(re_dict.keys())

    def process_rank(self, html):
        for key in self.re_dict.keys():
            if re.search(self.re_dict[key], html):
                return key - 1
        return self.worst

    def process_link(self, link, page_priority):
        href = str(link["href"])
        for key in self.re_dict.keys():
            innerHTML = str(link.decode_contents(formatter="html"))
            regexp = self.re_dict[key]
            if (
                    re.search(regexp, innerHTML, re.IGNORECASE) or
                    re.search(regexp, href, re.IGNORECASE)
                    ):
                return (key, href)
        if page_priority < self.worst:
            return (page_priority, href)
        else:
            return (self.worst, href)

    def process_page(self, html, queue_ref):
        page_rank = self.process_rank(html)
        soup = BeautifulSoup(html, "lxml")
        links = soup.find_all("a", href=True)
        for link in links:
            href = str(urllib.parse.urljoin(self.baseurl, link["href"]))
            lnk = self.prioritizer.prioritize_link(link, page_priority)
            self.queu_ref.put(lnk)


class SearchMaster(threading.Thread):
    def __init__(self, shared_queue, signal_stop, regex_dict, eng = None,
            dbhandler = core.MongoDBDump(), nthreads = 8,
            group = None, name = None, args = (), kwargs = None):
        super(SearchMaster, self).__init__(group = group, name = name,
                args = args, kwargs = kwargs)
        if not eng:
            self.eng = core.TorEngine()
        else:
            self.eng = eng
        self.db = dbhandler
        self.signal_stop = signal_stop
        self.nthreads = nthreads
        self.queue_ref = shared_queue
        self.prioritizer = Prioritizer(regex_dict)

        if not isinstance(shared_queue, queue.PriorityQueue):
            raise errors.MasterError("must use PriorityQueue for argument 2")

        # PriorityQueues are thread safe
        self.queue_ref = shared_queue

    def notify(self, message):
        if isinstance(message, core.Website):
            threading.Thread(target=self.db.dump(message))
            threading.Thread(target=self.prioritizer.process_page(
                str(message.source), self.queue_ref
                ))
            return True
        elif isinstance(message, core.IncompleteWebsite):
            pass
            # in the future think about processing "incomplete" sites
            #threading.Thread(target=self.db.dump(
            #    core.Website(message.url, "")
            #    ))
        elif isinstance(message, core.CheckDuplicate):
            if db.find_by_id(message.url).limit(1).count() > 0:
                return True
            else:
                return False
        else:
            return False

    def run(self):
        children = []
        for x in range(0, self.nthreads):
            t = SearchWorker(self)
            children.append(t)
            t.start()

        for t in children:
            t.join()
