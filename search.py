import threading
import queue
import urllib
import dagur.core
import dagur.crawler
from bs4 import BeautifulSoup
import sys

regex_dict = {
        1: "(barely\s*legal|escort)",
        2: "good\s*time"
        }
eng = dagur.core.TorEngine(pw="Cyclohexa-135-triene")
db = dagur.core.MongoDBDump("127.0.0.1","27017", "crawler", "search")
shared_queue = queue.PriorityQueue()
stop = threading.Event()
onion_gen = dagur.crawler.OnionGenWrapper(shared_queue, stop)
master = dagur.crawler.SearchMaster(shared_queue, stop, regex_dict, eng = eng)
onion_gen.start()
master.start()
